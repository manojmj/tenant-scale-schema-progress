require 'yaml'
require 'erb'
require 'json'
require_relative 'helper.rb'

Dir.chdir "gitlab"


def table_growth_data
  puts Dir.pwd
  data = File.readlines('../scripts/sharding_keys/table-growth.txt').map(&:chomp)

  # Parse data into a format suitable for Chart.js
  parsed_data = data.map do |line|
    date, count = line.split(': ')
    { date: date, count: count.to_i }
  end
end

def html
  template = %{
  <!DOCTYPE html>
  <html>
    <head>
      <meta charset="utf-8">
      <meta name="generator" content="GitLab Pages">
      <title>Tenant Scale Progress - Sharding Keys</title>
      <meta http-equiv="Refresh" content="0; URL=https://gitlab-org.gitlab.io/tenant-scale-group/cells-progress-tracker/sharding_keys" />

      <link rel="stylesheet" href="style.css">
      <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@0.1.1"></script>
    </head>
    <body>
      <div class="contribute-link-container">
          <a class="contribute-link" href="https://gitlab.com/manojmj/tenant-scale-schema-progress" target="_blank">Contribute</a>
          <a class="contribute-link" href="https://manojmj.gitlab.io/tenant-scale-schema-progress">Go to Schema Progress</a>
      </div>

      <div class="content-container">

        <h1>Progress of Sharding Key migration work for Cells</h1>

        <small>
          This dashboard is used to track progress of the sharding key migration work related to <a href=https://gitlab.com/groups/gitlab-org/-/epics/11670>Organization Isolation</a><br>
          <a href=https://gitlab.com/gitlab-org/gitlab/-/issues/429184>Issue</a>

          <br>
          Last updated at: #{Time.now.utc}
        </small>

        <h3>  Progress </h3>

          <h5> Total number of tables requiring sharding key/desired sharding key: <%= total_progress_data.total_tables %> </h5>
          <small> This includes tables with schemas in one of: gitlab_main, gitlab_main_cell, gitlab_ci, gitlab_pm </small>
          <h5> Total number of tables having sharding key/desired sharding key set: <%= total_progress_data.completed_tables %></h5>
          <h5> Percentage of work completed: <%= total_progress_data.percentage_completed %>%</h5>

          <div style="text-align: center;">
            <div style="width: 500px; height: 200px;">
              <canvas id="piechart"></canvas>
            </div>
          </div>

        <h3> Progress over time </h3>
          <small> This graph shows the growth of number of tables having sharding key data set over time</small>
          <div style="text-align: center;">
            <div style="width: 500px; height: 350px;">
              <canvas id="table_growth"></canvas>
            </div>
          </div>

        <h3> Progress per feature category </h3>
          <table>
            <tr>
              <th>Feature category</th>
              <th>Percentage completed</th>
              <th>Sharding key automation run executed?</th>
              <th>Desired sharding key automation run executed?</th>
            </tr>

            <% data_grouped_by_feature_category.each do |feature_category_data|  %>
              <tr>
                <td><%= feature_category_data.feature_category_name %></td>
                <td><%= feature_category_data.percentage_completed %>%</td>
                <td><%= feature_category_data.sharding_key_automation_run_executed_string %></td>
                <td><%= feature_category_data.desired_sharding_key_automation_run_executed_string %></td>
              </tr>
            <% end %>
          </table>

          <br>

        <h3> Progress of desired sharding key backfill migrations </h3>
          <p> This table is built based on the state of the database at <%= desired_sharding_key_migrations_last_updated_at %><p>

          <table>
            <tr>
              <th>Migration class</th>
              <th>Status</th>
              <th>Started at</th>
              <th>Finished at</th>
              <th>Estimated end at</th>
              <th>Progress</th>
            </tr>

            <% desired_sharding_key_migrations.each do |data|  %>
              <tr>
                <td><%= data.desired_sharding_key_migration_job_with_link %></td>
                <td><%= data.status %></td>
                <td><%= data.started_at %></td>
                <td><%= data.finished_at %></td>
                <td><%= data.estimated_end_at %></td>
                <td><%= data.progress %>%</td>
              </tr>
            <% end %>
          </table>


        <br>


        <h3> Details per feature category </h3>
        <br>
        <% data_grouped_by_feature_category.each do |feature_category_data|  %>
          <details><summary> <%= feature_category_data.feature_category_name %> - <%= feature_category_data.completed_string %> <%= feature_category_data.emoji %> </summary>

          <table>
            <tr>
              <th>Number</th>
              <th>Table name</th>
              <th>gitlab_schema</th>
              <th>Sharding key</th>
              <th>Desired sharding key</th>
              <th>Work complete?</th>
              <th>Work to be done</th>
            </tr>

            <% feature_category_data.entries.each.with_index(1) do |data, index|  %>
              <tr>
                <td><%= index %></td>
                <td><%= data.full_link %></td>
                <td><%= data.gitlab_schema %></td>
                <td><%= data.sharding_key_key_name %></td>
                <td><%= data.desired_sharding_key_key_name %></td>
                <td><%= data.sharding_key_work_complete? ? "Yes" : "No" %></td>
                <td><%= data.sharding_key_work_to_be_done_string %></td>
              </tr>
            <% end %>
          </table>
          </details>
        <% end  %>
      </div>

      <script>
        var percentage_a = <%= percentage(total_progress_data.completed_tables, total_progress_data.total_tables) %>;
        var percentage_b = <%= percentage(total_progress_data.incomplete_tables, total_progress_data.total_tables) %>;

        var ctx = document.getElementById("piechart").getContext("2d");
        var pieChart = new Chart(ctx, {
          type: "pie",
          height: 50,
          width: 40,
          data: {
            labels: ["sharding key/desired sharding key set", "no sharding key"],
            datasets: [
              {
                data: [percentage_a, percentage_b],
                backgroundColor: ["#34a853", "#EA4335"],
              },
            ],
          },
        });

        var data = <%= table_growth_data.to_json %>

        var dates = data.map(item => new Date(item.date));
        var counts = data.map(item => item.count);

        var table_growth_ctx = document.getElementById("table_growth").getContext("2d");

        const myChart = new Chart(table_growth_ctx, {
            type: "line",
            data: {
                labels: dates, // X-axis data (dates)
                datasets: [{
                    label: "Counts",
                    data: counts, // Y-axis data (counts)
                    borderColor: "rgb(75, 192, 192)", // Line color
                    fill: false, // Don't fill the area under the line
                }]
            },
            options: {
                scales: {
                    x: {
                        type: 'time',
                        time: {
                            unit: 'month',
                            displayFormats: {
                              month: 'MMM YYYY' // Customize the date format
                          },
                        },
                    },
                    y: {
                        beginAtZero: false,
                    }
                }
            }
        });
      </script>
    </body>
  </html>
  }.gsub(/^  /, '')

  rhtml = ERB.new(template)
  rhtml.result
end

File.open('../public/sharding_keys.html', 'w+') { |file| file.write(html) }

Dir.chdir "."
