require 'net/http'
require 'json'
require 'active_support/all'

class Entry
  def initialize(file_path)
    @file_path = file_path
    @data = YAML.load_file(file_path)
  end

  def name_and_schema
    [key_name, gitlab_schema.to_sym]
  end

  def table_name
    data['table_name']
  end

  def feature_categories
    data['feature_categories']
  end

  def view_name
    data['view_name']
  end

  def milestone
    data['milestone']
  end

  def gitlab_schema
    data['gitlab_schema']
  end

  def sharding_key
    data['sharding_key']
  end

  def desired_sharding_key
    data['desired_sharding_key']
  end

  def classes
    data['classes']
  end

  def schema?(schema_name)
    gitlab_schema == schema_name.to_s
  end

  def key_name
    table_name || view_name
  end

  def full_link
    "<a href=https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/docs/#{key_name}.yml target='_blank'> #{key_name} </a>"
  end

  def schema_needs_sharding?
    schema?('gitlab_main') || schema?('gitlab_main_cell') || schema?('gitlab_ci') || schema?('gitlab_pm')
  end

  def desired_sharding_key_key_name
    return "-" if desired_sharding_key.nil?

    desired_sharding_key.first.first
  end

  def sharding_key_key_name
    return "-" if sharding_key.nil?

    sharding_key.first.first
  end

  def sharding_key_work_complete?
    return true if !schema_needs_sharding?
    return true if !sharding_key.nil? || !desired_sharding_key.nil?

    return false
  end

  def sharding_key_work_to_be_done_string
    return "-" if sharding_key_work_complete?

    work = []

    work << "Change schema from gitlab_main." if schema?('gitlab_main')
    work << "Set a sharding_key (or desired_sharding_key)"

    work.join("<br>")
  end
end

class PerFeatureCategory
  def initialize(feature_category_name, entries)
    @feature_category_name = feature_category_name
    @entries = entries
    @sharding_key_mr_link = nil
    @desired_sharding_key_mr_link = nil
    @mr_status = nil
  end

  attr_reader :feature_category_name

  def entries
    @entries.sort_by { |entry| entry.sharding_key_work_complete? ? 0 : 1 }
  end

  def total_count
    entries.count
  end

  def completed_count
    @entries.select { |e| e.sharding_key_work_complete? }.count
  end

  def percentage_completed
    ((completed_count.to_f / total_count.to_f) * 100.0).round(2)
  end

  def completed_string
    "#{percentage_completed}% completed"
  end

  def emoji
    percentage_completed == 100.0 ? "<span>&#10003;</span>" : ''
  end

  def sharding_key_automation_run_executed?
    branch_name = "determine-sharding-key-feature-category-#{feature_category_name}"

    any_mr_exists?(branch_name, '@sharding_key_mr_link')
  end

  def desired_sharding_key_automation_run_executed?
    branch_name = "determine-desired-sharding-key-feature-category-#{feature_category_name}"

    any_mr_exists?(branch_name, '@desired_sharding_key_mr_link')
  end

  def any_mr_exists?(branch_name, key)
    uri = URI("https://gitlab.com/api/v4/projects/278964/merge_requests?source_branch=#{branch_name}")
    res = Net::HTTP.get_response(uri)

    if res.body == "[]"
      return false
    else
      json = JSON.parse(res.body)
      instance_variable_set(key.to_sym, json.first['web_url'])
      instance_variable_set(:@mr_status, json.first['state'])
    end
  end

  def mr_status_wording
    status = @mr_status
    status[0] = status[0].upcase

    "(#{status})"
  end

  def sharding_key_automation_run_executed_string
    if sharding_key_automation_run_executed?
      return "Yes <span>&#10003;</span> <a href=#{@sharding_key_mr_link} target='_blank'>#{mr_status_wording}</a>"
    end

    if percentage_completed == 100.0
      return "Reached 100% coverage, execution not necessary"
    end

    if sharding_key_automation_wont_yield_any_result?
      return "Automation won't yield any sharding keys"
    end

    "Pending to be run"
  end

  def desired_sharding_key_automation_run_executed_string
    if desired_sharding_key_automation_run_executed?
      return "Yes <span>&#10003;</span> <a href=#{@desired_sharding_key_mr_link} target='_blank'>#{mr_status_wording}</a>"
    end

    if percentage_completed == 100.0
      return "Reached 100% coverage, execution not necessary"
    end

    if desired_sharding_key_automation_wont_yield_any_result?
      return "Automation won't yield any desired sharding keys"
    end

    "Pending to be run"
  end

  private

  def desired_sharding_key_automation_wont_yield_any_result?
    categories_that_cannot_set_any_desired_sharding_keys.include?(feature_category_name.to_s)
  end

  def sharding_key_automation_wont_yield_any_result?
    categories_that_cannot_set_any_sharding_keys.include?(feature_category_name.to_s)
  end

  def categories_that_cannot_set_any_desired_sharding_keys
    ["application_instrumentation",
     "audit_events",
     "auto_devops",
     "build_artifacts",
     "cell",
     "cloud_connector",
     "code_suggestions",
     "consumables_cost_management",
     "container_registry",
     "database",
     "dependency_management",
     "dependency_proxy",
     "devops_reports",
     "duo_chat",
     "environment_management",
     "fleet_visibility",
     "fuzz_testing",
     "gitaly",
     "global_search",
     "groups_and_projects",
     "insider_threat",
     "instance_resiliency",
     "integrations",
     "onboarding",
     "pipeline_composition",
     "purchase",
     "navigation",
     "release_evidence",
     "release_orchestration",
     "requirements_management",
     "runner",
     "seat_cost_management",
     "sm_provisioning",
     "service_desk",
     "software_composition_analysis",
     "subscription_management",
     "system_access",
     "incident_management",
     "unknown_category",
     "metrics",
     "error_tracking",
     "jihu",
     "web_ide"]
  end

  def categories_that_cannot_set_any_sharding_keys
    ["audit_events",
    "cell",
    "cloud_connector",
    "code_suggestions",
    "code_testing",
    "compliance_management",
    "consumables_cost_management",
    "container_registry",
    "database",
    "dependency_proxy",
    "deployment_management",
    "design_management",
    "devops_reports",
    "duo_chat",
    "dynamic_application_security_testing",
    "environment_management",
    "feature_flags",
    "fleet_visibility",
    "fuzz_testing",
    "global_search",
    "groups_and_projects",
    "infrastructure_as_code",
    "insider_threat",
    "instance_resiliency",
    "integrations",
    "mlops",
    "onboarding",
    "pages",
    "pipeline_composition",
    "portfolio_management",
    "release_evidence",
    "release_orchestration",
    "requirements_management",
    "runner",
    "seat_cost_management",
    "secrets_management",
    "security_policy_management",
    "sm_provisioning",
    "software_composition_analysis",
    "subscription_management",
    "team_planning",
    "user_profile",
    "value_stream_management",
    "web_ide",
    "jihu",
    "unknown_category",
    "wiki",
    "service_desk",
    "incident_management",
    "metrics",
    "error_tracking"]
  end
end

class Progress
  def initialize(data_grouped_by_feature_category)
    @data_grouped_by_feature_category = data_grouped_by_feature_category
  end

  def total_tables
    data_grouped_by_feature_category.sum { |pfc| pfc.entries.select { |entry| entry.schema_needs_sharding? }.count }
  end

  def completed_tables
    data_grouped_by_feature_category.sum { |pfc| pfc.entries.select { |entry| entry.sharding_key_work_complete? }.count }
  end

  def incomplete_tables
    total_tables - completed_tables
  end

  def percentage_completed
    ((completed_tables.to_f / total_tables.to_f) * 100.0).round(2)
  end
end


def data
  @data ||= Dir.glob("db/docs/*.yml").map do |file|
    Entry.new(file)
  end
end

def data_grouped_by_feature_category
  @grouped = Hash.new { |h, k| h[k] = [] }
  data.each do |entry|
    category = entry.feature_categories.first || 'unknown_category'
    @grouped[category] << entry
  end

  @grouped.map {|category_name, entries| PerFeatureCategory.new(category_name, entries) }
    .sort_by{ |pfc| -pfc.percentage_completed }
end

def total_progress_data
  Progress.new(data_grouped_by_feature_category)
end

def percentage(value, total)
  ((value.to_f / total.to_f) * 100.0).round(2)
end

class DesiredShardingKeyProgressData
  def initialize
    @data = YAML.load_file('../scripts/sharding_keys/desired_sharding_key_progress_data.yml')
  end

  class Migration
    def initialize(migration)
      @migration = migration
    end

    def finished_at
      @migration['finished_at']
    end

    def started_at
      @migration['started_at']
    end

    def estimated_end_at
      @migration['estimated_end_at']
    end

    def progress
      @migration['progress']
    end

    def status
      @migration['status']
    end

    def desired_sharding_key_migration_job_name
      @migration['desired_sharding_key_migration_job_name']
    end

    def desired_sharding_key_migration_job_with_link
      name = desired_sharding_key_migration_job_name.tableize.singularize
      link = "https://gitlab.com/gitlab-org/gitlab/-/blob/master/db/docs/batched_background_migrations/#{name}.yml"

      "<a href=#{link} target='_blank'>#{desired_sharding_key_migration_job_name}</a>"
    end
  end

  def last_updated_at
    @data['last_updated_at']
  end

  def migrations
    @data['migrations'].map { |entry| Migration.new(entry) }
  end
end


def desired_sharding_key_migrations
  DesiredShardingKeyProgressData.new.migrations
end

def desired_sharding_key_migrations_last_updated_at
  DesiredShardingKeyProgressData.new.last_updated_at
end
