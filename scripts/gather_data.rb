#!/usr/bin/env ruby

require 'erb'
require 'json'
require_relative 'data_repo.rb'

def percentage(value, total)
  ((value.to_f / total.to_f) * 100.0).round(2)
end

def formatted_data(value, total)
  "#{value} tables, #{ percentage(value, total)}% of total"
end

def build_link(file, line_number)
  "<a href=https://gitlab.com/gitlab-org/gitlab/-/blob/master/#{file}#L#{line_number} target='_blank'>#{file}:#{line_number}</a>"
end

def cross_join_counts
  grep_word_in_files.sum { |_, line_numbers| line_numbers.size }
end

def cross_db_foreign_key_counts
  grep_for_cross_db_fk_issues.sum { |_, line_numbers| line_numbers.size }
end

def grep_word_in_files(search_word = '.allow_cross_joins_across_databases(url:')
  result = []

  Dir.glob("**/*.rb").each do |file|
    next if file.include?('_spec.rb') || file.include?('spec/support/database/prevent_cross_joins.rb')

    line_number = 0
    found_lines = []

    File.open(file, 'r').each_line do |line|
      line_number += 1
      found_lines << line_number if line.include?(search_word)
    end

    result << [file, found_lines] unless found_lines.empty?
  end

  result
end

def grep_for_cross_db_fk_issues(search_word = 'https://gitlab.com/gitlab-org/gitlab/-/issues')
  result = []

  line_number = 0
  found_lines = []

  file = 'spec/lib/gitlab/database/no_cross_db_foreign_keys_spec.rb'

  File.open(file, 'r').each_line do |line|
    line_number += 1
    found_lines << line_number if line.include?(search_word)
  end

  result << [file, found_lines] unless found_lines.empty?

  result
end

def table_growth_data
  puts Dir.pwd
  data = File.readlines('../scripts/table-growth.txt').map(&:chomp)

  # Parse data into a format suitable for Chart.js
  parsed_data = data.map do |line|
    date, count = line.split(': ')
    { date: date, count: count.to_i }
  end
end

template = %{
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="GitLab Pages">
    <title>Tenant Scale Progress</title>
    <meta http-equiv="Refresh" content="0; URL=https://gitlab-org.gitlab.io/tenant-scale-group/cells-progress-tracker/schema_migration" />
    <link rel="stylesheet" href="style.css">
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/moment@2.29.1/moment.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-adapter-moment@0.1.1"></script>
  </head>
  <body>
    <div class="contribute-link-container">
        <a class="contribute-link" href="https://gitlab.com/manojmj/tenant-scale-schema-progress" target="_blank">Contribute</a>
        <a class="contribute-link" href="https://manojmj.gitlab.io/tenant-scale-schema-progress/sharding_keys">Go to migration progress</a>
    </div>

    <div class="content-container">

      <h1>Progress of Cells Database schema migration work</h1>

      <details><summary>Tables in main, (yet to be classified) - (<%= formatted_data(gitlab_main_tables.count, total_size) %>)</summary>
        <ul>
        <% gitlab_main_tables.each do |table| %>
          <li><%= table %></li>
        <% end %>
        </ul>
      </details>

      <details><summary>Tables classified as main_clusterwide - (<%= formatted_data(gitlab_main_clusterwide_tables.count, total_size)%>)</summary>
        <ul>
        <% gitlab_main_clusterwide_tables.each do |table| %>
          <li><%= table %></li>
        <% end %>
        </ul>
      </details>


      <details><summary>Tables classified as main_cell - (<%= formatted_data(gitlab_main_cell_tables.count, total_size) %>)</summary>
        <ul>
        <% gitlab_main_cell_tables.each do |table| %>
          <li><%= table %></li>
        <% end %>
        </ul>
      </details>


      <p> Total number of tables (does not include CI tables): <%= total_size %> </p>


      <div style="text-align: center;">
        <div style="width: 200px; height: 200px;">
          <canvas id="piechart"></canvas>
        </div>
      </div>


      <h1>Outstanding cross-join issues</h1>

      <details><summary> Number of cross-join issues to be fixed: <%= cross_join_counts %></summary>
        <ul>
        <% grep_word_in_files.each do |file_path, line_numbers| %>
            <% line_numbers.each do |line_number| %>
              <li><%= build_link(file_path, line_number) %></li>
            <% end %>
        <% end %>
        </ul>
      </details>

      <h1>Outstanding cross-db foreign key issues</h1>

      <details><summary> Number of cross-db foreign key issues to be fixed: <%= cross_db_foreign_key_counts %></summary>
        <ul>
        <% grep_for_cross_db_fk_issues.each do |file_path, line_numbers| %>
            <% line_numbers.each do |line_number| %>
              <li><%= build_link(file_path, line_number) %></li>
            <% end %>
        <% end %>
        </ul>
      </details>

      <h1>Number of tables over time</h1>

      <details><summary> What does this graph signify?</summary>
        <p> This graph is used to assess how our "goal-post is moving" as new tables are being introduced </p>
        <p> that does not belong to either the `gitlab_main_cell` or `gitlab_main_clusterwide` schema. </p>
        <p> See <a href="https://gitlab.com/gitlab-org/gitlab/-/issues/424990">this</a> issue for details.<p>
        <p> PS: This does not include CI tables. </p>
      </details>

      <div style="text-align: center;">
        <div style="width: 500px; height: 500px;">
          <canvas id="table_growth"></canvas>
        </div>
      </div>
    </div>

    <script>
      var percentage_a = <%= percentage(gitlab_main_clusterwide_tables.count, total_size) %>;
      var percentage_b = <%= percentage(gitlab_main_cell_tables.count, total_size) %>;
      var percentage_c = <%= percentage(gitlab_main_tables.count, total_size) %>;

      var ctx = document.getElementById("piechart").getContext("2d");
      var pieChart = new Chart(ctx, {
        type: "pie",
        height: 50,
        width: 40,
        data: {
          labels: ["main_clusterwide", "main_cell", "main"],
          datasets: [
            {
              data: [percentage_a, percentage_b, percentage_c],
              backgroundColor: ["#34a853", "#4285F4", "#EA4335"],
            },
          ],
        },
      });

      var data = <%= table_growth_data.to_json %>

      var dates = data.map(item => new Date(item.date));
      var counts = data.map(item => item.count);

      var table_growth_ctx = document.getElementById("table_growth").getContext("2d");

      const myChart = new Chart(table_growth_ctx, {
          type: "line",
          data: {
              labels: dates, // X-axis data (dates)
              datasets: [{
                  label: "Counts",
                  data: counts, // Y-axis data (counts)
                  borderColor: "rgb(75, 192, 192)", // Line color
                  fill: false, // Don't fill the area under the line
              }]
          },
          options: {
              scales: {
                  x: {
                      type: 'time',
                      time: {
                          unit: 'month',
                          displayFormats: {
                            month: 'MMM YYYY' // Customize the date format
                        },
                      },
                  },
                  y: {
                      beginAtZero: false,
                  }
              }
          }
      });
    </script>

  </body>
</html>
}.gsub(/^  /, '')

rhtml = ERB.new(template)

html = rhtml.result

# File.open('index.html', 'w+') { |file| file.write(html) }

File.open('../public/index.html', 'w+') { |file| file.write(html) }

Dir.chdir "."
