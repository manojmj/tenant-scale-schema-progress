#!/usr/bin/env ruby

def latest_table_count
  File.foreach('scripts/table-growth.txt').first
end

def existing_count
  latest_table_count.split(':').last.strip.to_i
end

def file_prepend(file, str)
  new_contents = ""
  File.open(file, 'r') do |fd|
    contents = fd.read
    new_contents = str << contents
  end
  # Overwrite file but now with prepended string on it
  File.open(file, 'w') do |fd|
    fd.write(new_contents)
  end
end

def write_new_count
  date = Time.now.strftime("%Y-%m-%d")

  value = "#{date}: #{new_count}"

  file_prepend("scripts/table-growth.txt", "#{value}\n")
end

def new_count
  ARGV[0].to_i
end

write_new_count if existing_count != new_count

